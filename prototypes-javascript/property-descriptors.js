let person = { name: "farhan apriliandi" };

// let objectBase = Object.getPrototypeOf(person);

/*
   Result :

   {
      value: [Function: toString],
      writable: true, => kalau true bisa diubah nilainya dan sebaliknya
      enumerable: false, 
      configurable: true => bisa dihapus properti-nya dan sebaliknya
   }
*/
// let descriptor = Object.getOwnPropertyDescriptor(objectBase, 'toString');


Object.defineProperty(person, 'name', {
   writable: false,
   enumerable: true,
   configurable: false
});

person.name = "parhan";
delete person.name;

console.log(person);