// Before
// function Circle(radius) {
//    this.radius = radius;

//    this.draw = function () {
//       console.log('draw');
//    }
// }

// const c1 = new Circle(1);
// const c2 = new Circle(1);

// ketika kita membuat object, maka kita akan mengcopy methodnya, dan bila kita membuat banyak objek akan banyak memakan memori.
// maka dari itu kita buat methodnya menjadi prototype

// After
function Circle(radius) {
   // instance members
   this.radius = radius;

   this.move = function () {
      console.log("move");
      this.draw();
   };

   this.printName = function () {
      console.log(this.name);
   };
}

// prototype members
Circle.prototype.draw = function () {
   console.log("draw");
};

Circle.prototype.name = "Circle";

const c1 = new Circle(1);
const c2 = new Circle(1);

// object.hasOwnProperty(property / method);
// untuk mengetahui apakah property atau method itu punya si objek atau punya prototype.
// bila true maka punya si objek, dan bila false punya si prototype

const radiusProperty = c1.hasOwnProperty("radius");
console.log(radiusProperty);

const moveMethod = c1.hasOwnProperty("move");
console.log(moveMethod);

const drawMethod = c1.hasOwnProperty("draw");
console.log(drawMethod);

const nameProperty = c1.hasOwnProperty("name");
console.log(nameProperty);
