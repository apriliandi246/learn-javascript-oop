- Prototype is essentially a "parent" of another object.

- Every object in javascript, except a single object has prototype and it inherits all the members in it's prototype.

- Every object that we create in javascript directly or indirectly inherits from object base (prototype). Object base (prototype) is the root of all objects in javascript and it doesn't have a prototype or parent.
