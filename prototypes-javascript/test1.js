function Person(name, age) {
   this.name = name;
   this.age = age;

   Object.defineProperty(this, 'name', {
      writable: false,
      enumerable: true,
      configurable: false
   });

   Object.defineProperty(this, 'age', {
      writable: false,
      enumerable: true,
      configurable: false
   });
}

const person1 = new Person('sophie', 19);

person1.name = "farhan";
delete person1.name;

person1.age = 20;

console.log(person1);