function Time(pastDate) {
   Object.defineProperty(this, 'now', {
      value: new Date().getTime(),
      writable: false,
      enumerable: true,
      configurable: false
   });

   Object.defineProperty(this, 'past', {
      value: new Date(pastDate),
      writable: false,
      enumerable: true,
      configurable: false
   });
}

Time.prototype.getNameOfDay = function (numberOfDay) {
   const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

   return days[numberOfDay];
}

Time.prototype.getNameOfMonth = function (numberOfMonth) {
   const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

   return months[numberOfMonth];
}

Time.prototype.format = function (level) {
   let date = this.past.getDate();
   let year = this.past.getFullYear();
   let day = this.getNameOfDay(this.past.getDay());
   let month = this.getNameOfMonth(this.past.getMonth());

   if (level === 'hard') return `${day}, ${month} ${date}, ${year}`;
   if (level === 'medium') return `${month} ${date}, ${year}`;
   if (level === 'easy') return `${month} ${year}`;

   return new Error('Level Not Found');
}

Time.prototype.fromNow = function () {
   let difference = (this.now / 1000) - (this.past / 1000);
   let hour = Math.floor(difference / 3600);
   let diff = difference - (hour * 3600);
   let minute = Math.floor(diff / 60);

   let day = Math.floor(hour / 24);
   let week = Math.floor(day / 7);
   let month = Math.floor(week / 4.345);
   let year = Math.floor(month / 12);

   let result;

   if (year > 0) return result = (year === 1 ? 'a year ago' : `${year} years ago`);
   if (month > 0) return result = (month === 1 ? 'a month ago' : `${month} months ago`);
   if (week > 0) return result = (week === 1 ? 'a week ago' : `${week} weeks ago`);
   if (day > 0) return result = (day === 1 ? 'a day ago' : `${day} days ago`);
   if (hour > 0) return result = (hour === 1 ? 'an hour ago' : `${hour} hours ago`);
   if (minute > 0) return result = (minute === 1 ? 'a minute ago' : `${minute} minutes ago`);

   return 'a few seconds ago';
}


const time = new Time('2020-08-04T15:23:14.572Z');

console.log(time.fromNow());
console.log(time.format('hard'));
console.log(time.format('medium'));
console.log(time.format('easy'));


