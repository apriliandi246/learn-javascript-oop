// example static method in build in javascript is Math class
// we directly call method using static method

class Circle {
   constructor(radius) {
      this.radius = radius;
   }

   // instance method
   draw() {

   }

   // static method
   static parse(str) {
      const radius = JSON.parse(str).radius;
      return new Circle(radius);
   }
}

const circle = Circle.parse('{"radius": 1}');
console.log(circle);
