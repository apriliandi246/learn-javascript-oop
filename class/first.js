class Circle {
   constructor(radius) {
      Object.defineProperty(this, 'radius', {
         value: radius,
         writable: false,
         enumerable: true,
         configurable: false
      });
   }

   draw() {
      console.log('draw');
   }
}

const c = new Circle(1);

console.log(c.radius);