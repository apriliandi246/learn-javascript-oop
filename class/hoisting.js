sayHello();

// function declaration
function sayHello() {
   console.log('hello');
}

// function expression
const sayGoodbye = function () {
   console.log('goodbye');
}

sayGoodbye();




// class declaration is not hoisted
class Circle {
}

// class expression is not hoisted
const Square = class {
}