const _radius = Symbol();

class Circle {
   #name;

   constructor(radius, name) {
      this[_radius] = radius;
      this.#name = name;
   }
}

const c = new Circle(1, "farhan");
console.log(c.radius);
console.log(c.#name);