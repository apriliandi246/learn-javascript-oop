class Person {
   constructor(name, age, address) {
      this.name = name;
      this.age = age;
      this.address = address;
   }
}

class Programmer extends Person {
   #skills;

   constructor(name, age, address, skills) {
      super(name, age, address, skills);
      this.#skills = skills;
   }

   getName() {
      return this.name;
   }

   getSkills() {
      console.log(this["__#26205@#getName"]);
      console.log(this.#skills);
   }

   #getPassword() {
      console.log("you fucking asshole");
   }
}

const p = new Programmer("farhan", 20, "Indonesia", ["web development"]);

p.getSkills();

console.log(p.skills);

p.#getPassword();
