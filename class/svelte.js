function generateRandomNumber() {
   console.log(Math.random());
}

class Person {
   constructor(name, age) {
      this.name = name;
      this.age = age;
   }

   init(generateRandomNumber) {
      generateRandomNumber();
   }
}

const person = new Person("farhan", 20);

console.log(person.name);
console.log(person.age);
person.init(generateRandomNumber);

function printPerson(person) {
   return {
      printName() {
         console.log(person.name);
      },

      printAge() {
         console.log(person.age);
      },
   };
}

const data = printPerson(person);
data.printName();
data.printAge();
